from sqlalchemy import (
        create_engine,
        Column,
        Integer,
)
from sqlalchemy import exc
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import tornado.ioloop
import tornado.web

#SQLALCHEMY_DATABASE_URI = 'sqlite:///fallback.db'
SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://app:apppassword@dev-db.sean-internal.colab.cloudreach.com:3306/app'
try:
    engine = create_engine(SQLALCHEMY_DATABASE_URI, echo=True)
except exc.SQLAlchemyError as e:
    engine = create_engine('sqlite://fallback.db')
    fallback_database = True
Base = declarative_base()
Session = sessionmaker(bind=engine)


class Count(Base):
    __tablename__ = "databate_queries"

    id = Column(Integer, primary_key=True)
    count = Column(Integer)

    def __repr__(self):
        return "<Count(count={!r})>".format(self.count)


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        session = Session()
        count = session.query(Count, Count.count).order_by(Count.count.desc()).first()
        if not count:
            session.add(Count(count=1))
            session.commit()
            count = session.query(Count, Count.count).order_by(Count.count.desc()).first()
        session.add(Count(count=count.count+1))
        session.commit()
        self.write("Hello, world\nQuery No: {}\n".format(count.count))


class StatusHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Healthy")


def make_app():
    return tornado.web.Application([
        (r"/", MainHandler),
        (r"/status", StatusHandler)
    ])


if __name__ == "__main__":
    Base.metadata.create_all(engine)
    Session.configure(bind=engine)
    app = make_app()
    app.listen(5000)
    tornado.ioloop.IOLoop.current().start()
